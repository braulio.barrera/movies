﻿using System;
using System.Collections.Generic;

namespace Data.Models.DB
{
    public partial class MovieReservation
    {
        public int ReservationId { get; set; }
        public DateTime ReservationFrom { get; set; }
        public DateTime ReservationTo { get; set; }
        public bool? Returned { get; set; }
        public int UserId { get; set; }
        public int MovieId { get; set; }

        public Movies Movie { get; set; }
        public Users User { get; set; }
    }
}
