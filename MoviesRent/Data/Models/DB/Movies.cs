﻿using System;
using System.Collections.Generic;

namespace Data.Models.DB
{
    public partial class Movies
    {
        public Movies()
        {
            MovieReservation = new HashSet<MovieReservation>();
        }

        public int Id { get; set; }
        public string Title { get; set; }
        public decimal MovieYear { get; set; }
        public string DirectorName { get; set; }
        public double Rating { get; set; }
        public bool? Available { get; set; }
        public string Actors { get; set; }
        public int GenreId { get; set; }

        public Genre Genre { get; set; }
        public ICollection<MovieReservation> MovieReservation { get; set; }
    }
}
