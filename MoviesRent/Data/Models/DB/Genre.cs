﻿using System;
using System.Collections.Generic;

namespace Data.Models.DB
{
    public partial class Genre
    {
        public Genre()
        {
            Movies = new HashSet<Movies>();
        }

        public int Id { get; set; }
        public string Name { get; set; }

        public ICollection<Movies> Movies { get; set; }
    }
}
