﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Data.Models.DB
{
    public partial class SapiensDbContext : DbContext
    {
        public SapiensDbContext()
        {
        }

        public SapiensDbContext(DbContextOptions<SapiensDbContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Genre> Genre { get; set; }
        public virtual DbSet<MovieReservation> MovieReservation { get; set; }
        public virtual DbSet<Movies> Movies { get; set; }
        public virtual DbSet<Users> Users { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer("Server=BRAULIO-PC;Database=SapiensDb;Trusted_Connection=True;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Genre>(entity =>
            {
                entity.Property(e => e.Name)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<MovieReservation>(entity =>
            {
                entity.HasKey(e => e.ReservationId);

                entity.Property(e => e.ReservationFrom).HasColumnType("datetime");

                entity.Property(e => e.ReservationTo).HasColumnType("datetime");

                entity.HasOne(d => d.Movie)
                    .WithMany(p => p.MovieReservation)
                    .HasForeignKey(d => d.MovieId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__MovieRese__Movie__182C9B23");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.MovieReservation)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__MovieRese__UserI__173876EA");
            });

            modelBuilder.Entity<Movies>(entity =>
            {
                entity.Property(e => e.Actors)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.DirectorName)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.MovieYear).HasColumnType("numeric(18, 0)");

                entity.Property(e => e.Title)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.Genre)
                    .WithMany(p => p.Movies)
                    .HasForeignKey(d => d.GenreId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Movies__GenreId__1273C1CD");
            });

            modelBuilder.Entity<Users>(entity =>
            {
                entity.HasKey(e => e.UserId);

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });
        }
    }
}
