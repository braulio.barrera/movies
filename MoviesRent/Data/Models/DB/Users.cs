﻿using System;
using System.Collections.Generic;

namespace Data.Models.DB
{
    public partial class Users
    {
        public Users()
        {
            MovieReservation = new HashSet<MovieReservation>();
        }

        public int UserId { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }

        public ICollection<MovieReservation> MovieReservation { get; set; }
    }
}
