﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.DTO
{
    public class MovieReservationDTO
    {
        public int ReservationId { get; set; }
        public DateTime ReservationFrom { get; set; }
        public DateTime ReservationTo { get; set; }
        public bool Returned { get; set; }
        public int UserId { get; set; }
        public int MovieId { get; set; }
    }
}
