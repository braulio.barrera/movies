﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.DTO
{
    public class GenreDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
