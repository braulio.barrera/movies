﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.DTO
{
    public class MoviesDTO
    {
        public int Id { get; set; }        
        public string Title { get; set; }
        public int MoviewYear { get; set; }
        public int GenreId { get; set; }
        public string DirectorName { get; set; }
        public float Rating { get; set; }
        public bool Available { get; set; }
        public string Actors { get; set; }
    }
}
