﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.DTO
{
    public class UsersDTO
    {  
        public int UserId { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
    }
}
