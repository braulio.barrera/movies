﻿using Domain.DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Repositories
{
    public interface IMoviesRepository : IRepository<MoviesDTO>
    {
    }
}
