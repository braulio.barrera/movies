﻿using Domain.DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Repositories
{
    public interface IUsersRepository : IRepository<UsersDTO>
    {
    }
}
