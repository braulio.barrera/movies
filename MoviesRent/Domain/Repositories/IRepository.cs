﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Domain.Repositories
{
    public interface IRepository<TObjectDto> where TObjectDto : class
    {
        Task<List<TObjectDto>> ListAsync();
        Task<List<TObjectDto>> ListAsync(CancellationToken cancellationToken);

        TObjectDto FindById(object id);
        Task<TObjectDto> FindByIdAsync(object id);
        Task<TObjectDto> FindByIdAsync(CancellationToken cancellationToken, object id);

        IQueryable<TObjectDto> Query();

        void Add(TObjectDto objectDto);
        void Update(TObjectDto objectDto);
        void Remove(object id);
    }
}
