create database SapiensDb;
go

use SapiensDb;

create table Genre
(
Id int identity(1,1),
Name varchar(50),
primary key(Id)
)
go

create table Movies
(
Id int identity(1,1),
Title varchar(50) not null,
MovieYear numeric not null,
DirectorName varchar(50),
Rating float not null,
Available bit,
Actors varchar(500),
GenreId int not null,
primary key(Id),
foreign key(GenreId) references Genre(id)
)
go

create table Users
(
UserId int identity(1,1),
Name varchar(100) not null,
Email varchar(100) not null,
primary key(UserId)
)
go

create table MovieReservation
(
ReservationId int identity(1,1),
ReservationFrom Datetime not null,
ReservationTo Datetime not null,
Returned bit,
UserId int not null,
MovieId int not null,
primary key(ReservationId),
foreign key(UserId) references Users(UserId),
foreign key(MovieId) references Movies(Id)
)