﻿using AutoMapper;
using Data.Models;
using Data.Models.DB;
using Domain.DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace Logic.AutoMapper
{
    public class AutoMapperConfig
    {
        public static void AutoMapperConfiguration()
        {
            Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<MoviesDTO, Movies>().ReverseMap();
                cfg.CreateMap<GenreDTO, Genre>().ReverseMap();
                cfg.CreateMap<UsersDTO, Users>().ForMember(app => app.MovieReservation, opt => opt.Ignore());
                cfg.CreateMap<Users, UsersDTO>();
                cfg.CreateMap<MovieReservation, MovieReservationDTO>();
                cfg.CreateMap<MovieReservationDTO, MovieReservation>().ForMember(app => app.User, opt => opt.Ignore())
                .ForMember(app => app.Movie, opt => opt.Ignore());
            });
        }
    }
}
