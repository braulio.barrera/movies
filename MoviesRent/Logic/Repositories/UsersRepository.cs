﻿using Data;
using Data.Models;
using Data.Models.DB;
using Domain.DTO;
using Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Text;

namespace Logic.Repositories
{
    public class UsersRepository : Repository<UsersDTO, Users>, IUsersRepository
    {
        private readonly SapiensDbContext _context;

        public UsersRepository(SapiensDbContext context) : base(context)
        {
            this._context = context;
        }
    
    }
}
