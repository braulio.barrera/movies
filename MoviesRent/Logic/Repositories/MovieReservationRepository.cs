﻿using AutoMapper;
using Data;
using Data.Models;
using Data.Models.DB;
using Domain.DTO;
using Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Logic.Repositories
{
    public class MovieReservationRepository : Repository<MovieReservationDTO, MovieReservation>, IMovieReservationRepository
    {
        private readonly SapiensDbContext _context;

        public MovieReservationRepository(SapiensDbContext context) : base(context)
        {
            this._context = context;
        }
    }
}
