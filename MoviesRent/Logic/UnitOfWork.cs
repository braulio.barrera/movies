﻿using Data;
using Data.Models.DB;
using Domain;
using Domain.Repositories;
using Logic.Repositories;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Logic
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly SapiensDbContext context;

        private IMoviesRepository _moviesRepository;
        private IGenreRepository _genreRepository;
        private IUsersRepository _usersRepository;
        private IMovieReservationRepository _movieReservationRepository;

        public UnitOfWork()
        {
            context = new SapiensDbContext();
        }

        public IMoviesRepository MoviesRepository => _moviesRepository ?? (_moviesRepository = new MovieRepository(context));
        public IGenreRepository GenreRepository => _genreRepository ?? (_genreRepository = new GenreRepository(context));
        public IUsersRepository UsersRepository => _usersRepository ?? (_usersRepository = new UsersRepository(context));
        public IMovieReservationRepository MovieReservationRepository => _movieReservationRepository ?? (_movieReservationRepository = new MovieReservationRepository(context));

        public int SaveChanges()
        {
            return context.SaveChanges();
        }

        public Task<int> SaveChangesAsync()
        {
            return context.SaveChangesAsync();
        }

        public Task<int> SaveChangesAsync(CancellationToken cancellationToken)
        {
            return context.SaveChangesAsync(cancellationToken);
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
