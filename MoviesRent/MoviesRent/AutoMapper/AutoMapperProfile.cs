﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Logic.AutoMapper;

namespace MoviesRent.AutoMapper
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile() : this("MapperProfile")
        {
        }

        public AutoMapperProfile(string profileName) : base(profileName)
        {
            AutoMapperConfig.AutoMapperConfiguration();
        }
    }
}
