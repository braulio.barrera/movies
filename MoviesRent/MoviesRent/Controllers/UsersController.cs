﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Domain;
using Domain.DTO;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace MoviesRent.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly IUnitOfWork unitOfWork;

        public UsersController(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        [HttpGet]
        public async Task<IEnumerable<UsersDTO>> Get()
        {
            try
            {
                return await unitOfWork.UsersRepository.ListAsync();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetById(int id)
        {
            var user = await unitOfWork.UsersRepository.FindByIdAsync(id);
            if (user == null)
            {
                return NotFound();
            }
            return Ok(user);
        }


        [HttpPost]
        public async Task<IActionResult> Post([FromBody] UsersDTO user)
        {
            if (user == null)
            {
                return BadRequest();
            }

            unitOfWork.UsersRepository.Add(user);
            await unitOfWork.SaveChangesAsync();

            return Created($"user/{user.UserId}", user);

        }

        

        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, [FromBody] UsersDTO user)
        {
            if (user == null || user.UserId != id)
            {
                return BadRequest();
            }

            unitOfWork.UsersRepository.Update(user);
            await unitOfWork.SaveChangesAsync();

            return NoContent();

        }


        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            var user = await unitOfWork.UsersRepository.FindByIdAsync(id);
            if (user == null)
            {
                return NotFound();
            }

            unitOfWork.UsersRepository.Remove(id);
            await unitOfWork.SaveChangesAsync();

            return NoContent();
        }
    }
}