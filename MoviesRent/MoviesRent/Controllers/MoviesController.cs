﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Domain;
using Domain.DTO;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace MoviesRent.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MoviesController : ControllerBase
    {
        private readonly IUnitOfWork unitOfWork;

        public MoviesController(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        [HttpGet]
        public async Task<IEnumerable<MoviesDTO>> Get()
        {
            try
            {
                return await unitOfWork.MoviesRepository.ListAsync();
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetById(int id)
        {
            var type = await unitOfWork.MoviesRepository.FindByIdAsync(id);
            if (type == null)
            {
                return NotFound();
            }
            return Ok(type);
        }


        [HttpPost]
        public async Task<IActionResult> Post([FromBody] MoviesDTO movie)
        {
            if (movie == null)
            {
                return BadRequest();
            }

            unitOfWork.MoviesRepository.Add(movie);
            await unitOfWork.SaveChangesAsync();

            return Created($"movie/{movie.Id}", movie);

        }


        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, [FromBody] MoviesDTO movie)
        {
            if (movie == null || movie.Id != id)
            {
                return BadRequest();
            }

            unitOfWork.MoviesRepository.Update(movie);
            await unitOfWork.SaveChangesAsync();

            return NoContent();

        }


        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            var movie = await unitOfWork.MoviesRepository.FindByIdAsync(id);
            if (movie == null)
            {
                return NotFound();
            }

            unitOfWork.MoviesRepository.Remove(id);
            await unitOfWork.SaveChangesAsync();

            return NoContent();
        }

    }
}