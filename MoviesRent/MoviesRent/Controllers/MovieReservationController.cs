﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Domain;
using Domain.DTO;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace MoviesRent.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MovieReservationController : ControllerBase
    {
        private readonly IUnitOfWork unitOfWork;

        public MovieReservationController(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        [HttpGet]
        public async Task<IEnumerable<MovieReservationDTO>> Get()
        {
            try
            {
                return await unitOfWork.MovieReservationRepository.ListAsync();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetById(int id)
        {
            var type = await unitOfWork.MovieReservationRepository.FindByIdAsync(id);
            if (type == null)
            {
                return NotFound();
            }
            return Ok(type);
        }


        [HttpPost]
        public async Task<IActionResult> Post([FromBody] MovieReservationDTO movie)
        {
            if (movie == null)
            {
                return BadRequest();
            }

            unitOfWork.MovieReservationRepository.Add(movie);
            await unitOfWork.SaveChangesAsync();

            return Created($"movie/{movie.ReservationId}", movie);

        }


        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, [FromBody] MovieReservationDTO movie)
        {
            if (movie == null || movie.ReservationId != id)
            {
                return BadRequest();
            }

            unitOfWork.MovieReservationRepository.Update(movie);
            await unitOfWork.SaveChangesAsync();

            return NoContent();

        }


        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            var movie = await unitOfWork.MovieReservationRepository.FindByIdAsync(id);
            if (movie == null)
            {
                return NotFound();
            }

            unitOfWork.MovieReservationRepository.Remove(id);
            await unitOfWork.SaveChangesAsync();

            return NoContent();
        }
    }
}