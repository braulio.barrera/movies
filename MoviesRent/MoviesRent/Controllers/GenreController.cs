﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http.Cors;
using Domain;
using Domain.DTO;
using Microsoft.AspNetCore.Mvc;

namespace MoviesRent.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class GenreController : ControllerBase
    {
        private readonly IUnitOfWork unitOfWork;

        public GenreController(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        [HttpGet]
        //[EnableCors("AllowSpecificOrigin")]
        public async Task<IEnumerable<GenreDTO>> Get()
        {
            try
            {
                return await unitOfWork.GenreRepository.ListAsync();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetById(int id)
        {
            var genre = await unitOfWork.GenreRepository.FindByIdAsync(id);
            if (genre == null)
            {
                return NotFound();
            }
            return Ok(genre);
        }


        [HttpPost]
        public async Task<IActionResult> Post([FromBody] GenreDTO genre)
        {
            if (genre == null)
            {
                return BadRequest();
            }

            unitOfWork.GenreRepository.Add(genre);
            await unitOfWork.SaveChangesAsync();

            return Created($"genre/{genre.Id}", genre);

        }



        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, [FromBody] GenreDTO genre)
        {
            if (genre == null || genre.Id != id)
            {
                return BadRequest();
            }

            unitOfWork.GenreRepository.Update(genre);
            await unitOfWork.SaveChangesAsync();

            return NoContent();

        }


        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            var genre = await unitOfWork.GenreRepository.FindByIdAsync(id);
            if (genre == null)
            {
                return NotFound();
            }

            unitOfWork.GenreRepository.Remove(id);
            await unitOfWork.SaveChangesAsync();

            return NoContent();
        }
    }
}