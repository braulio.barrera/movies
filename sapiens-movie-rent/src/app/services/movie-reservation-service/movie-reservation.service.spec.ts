import { TestBed } from '@angular/core/testing';

import { MovieReservationService } from './movie-reservation.service';

describe('MovieReservationService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MovieReservationService = TestBed.get(MovieReservationService);
    expect(service).toBeTruthy();
  });
});
