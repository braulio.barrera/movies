import { Injectable } from '@angular/core';

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { MovieReservation } from './../../models/movie-reservation';

import { Observable } from 'rxjs';
import { map, catchError, tap } from 'rxjs/operators';
import { environment } from './../../../environments/environment';


@Injectable({
  providedIn: 'root'
})
export class MovieReservationService {
  private getMovieReservationsUrl = environment.SAPIENS_API + 'moviereservation';
  constructor(
    private http: HttpClient,
    public router: Router
  ) { }


  public getHeaders(): HttpHeaders {
    let httpHeaders = new HttpHeaders({
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Methods': '*',
      'Access-Control-Allow-Headers': '*',
      'Content-Type': 'application/json',
    });
    return httpHeaders;
  }

  public create(reservation: MovieReservation): Observable<any> {
    return this.http
      .post(this.getMovieReservationsUrl, reservation, {
        headers: this.getHeaders(),
      }).pipe(
        map((response: any) => {
          if (response.statusCode === 201) {
            return response.data;
          } else {
            return response;
          }
        })
      );
  }

  public update(reservation: MovieReservation): Observable<any> {
    return this.http
      .put(this.getMovieReservationsUrl + `/${reservation.reservationId}`, reservation, {
        headers: this.getHeaders(),
      }).pipe(
        map((response: any) => {
          if (response.statusCode === 204) {
            return response.data;
          } else {
            return response;
          }
        })
      );
  }

  public getReservations(): Observable<any> {
    return this.http
      .get(this.getMovieReservationsUrl, {
        headers: this.getHeaders(),
      }).pipe(
        map((response: any) => {
          if (response.statusCode === 201) {
            return response.data;
          } else {
            return response;
          }
        })
      );
  }
}
