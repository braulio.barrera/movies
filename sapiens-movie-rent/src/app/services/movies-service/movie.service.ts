import { Injectable } from '@angular/core';

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { Movie } from './../../models/movie';

import { Observable } from 'rxjs';
import { map, catchError, tap } from 'rxjs/operators';
import { environment } from './../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class MovieService {
  private getMovieUrl = environment.SAPIENS_API + 'movies';
  constructor(
    private http: HttpClient,
    public router: Router
  ) { }


  public getHeaders(): HttpHeaders {
    let httpHeaders = new HttpHeaders({
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Methods': '*',
      'Access-Control-Allow-Headers': '*',
      'Content-Type': 'application/json',
    });
    return httpHeaders;
  }

  public create(movie: Movie): Observable<any> {
    return this.http
      .post(this.getMovieUrl, movie, {
        headers: this.getHeaders(),
      }).pipe(
        map((response: any) => {
          if (response.statusCode === 201) {
            return response.data;
          } else {
            return response;
          }
        })
      );
  }

  public update(movie: Movie): Observable<any> {
    return this.http
      .put(this.getMovieUrl + `/${movie.id}`, movie, {
        headers: this.getHeaders(),
      }).pipe(
        map((response: any) => {
          if (response.statusCode === 204) {
            return response.data;
          } else {
            return response;
          }
        })
      );
  }

  public getMovies(): Observable<any> {
    return this.http
      .get(this.getMovieUrl, {
        headers: this.getHeaders(),
      }).pipe(
        map((response: any) => {
          if (response.statusCode === 201) {
            return response.data;
          } else {
            return response;
          }
        })
      );
  }
}
