import { Injectable } from '@angular/core';

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { User } from './../../models/user';

import { Observable } from 'rxjs';
import { map, catchError, tap } from 'rxjs/operators';
import { environment } from './../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private getUserUrl = environment.SAPIENS_API + 'users';
  constructor(
    private http: HttpClient,
    public router: Router
  ) { }


  public getHeaders(): HttpHeaders {
    let httpHeaders = new HttpHeaders({
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Methods': '*',
      'Access-Control-Allow-Headers': '*',
      'Content-Type': 'application/json',
    });
    return httpHeaders;
  }

  public create(user: User): Observable<any> {
    return this.http
      .post(this.getUserUrl, user, {
        headers: this.getHeaders(),
      }).pipe(
        map((response: any) => {
          if (response.statusCode === 201) {
            return response.data;
          } else {
            return response;
          }
        })
      );
  }

  public update(user: User): Observable<any> {
    return this.http
      .put(this.getUserUrl + `/${user.userId}`, user, {
        headers: this.getHeaders(),
      }).pipe(
        map((response: any) => {
          if (response.statusCode === 204) {
            return response.data;
          } else {
            return response;
          }
        })
      );
  }

  public getUsers(): Observable<any> {
    return this.http
      .get(this.getUserUrl, {
        headers: this.getHeaders(),
      }).pipe(
        map((response: any) => {
          if (response.statusCode === 201) {
            return response.data;
          } else {
            return response;
          }
        })
      );
  }
}
