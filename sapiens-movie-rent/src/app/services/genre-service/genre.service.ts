import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { Genre } from './../../models/genre';

import { Observable } from 'rxjs';
import { map, catchError, tap } from 'rxjs/operators';
import { environment } from './../../../environments/environment';



@Injectable({
  providedIn: 'root'
})
export class GenreService {
  private getGenresUrl = environment.SAPIENS_API + 'genre';
  constructor(
    private http: HttpClient,
    public router: Router
  ) { }


  public getHeaders(): HttpHeaders {
    let httpHeaders = new HttpHeaders({
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Methods': '*',
      'Access-Control-Allow-Headers': '*',
      'Content-Type': 'application/json',
    });
    return httpHeaders;
  }

  public create(genre: Genre): Observable<any> {
    return this.http
      .post(this.getGenresUrl, genre, {
        headers: this.getHeaders(),
      }).pipe(
        map((response: any) => {
          if(response.statusCode === 201){
            return response.data;
          } else {
            return response;
          }
        })
      );      
  }

  public update(genre: Genre): Observable<any>{
    return this.http
      .put(this.getGenresUrl + `/${genre.id}`, genre, {
        headers: this.getHeaders(),
      }).pipe(
        map((response: any) => {
          if(response.statusCode === 204){
            return response.data;
          } else {
            return response;
          }
        })
      );
  }

  public getGenres() : Observable<any>{
    return this.http
      .get(this.getGenresUrl,{
        headers: this.getHeaders(),
      }).pipe(
        map((response: any) => {
          if(response.statusCode === 201){
            return response.data;
          } else {
            return response;
          }
        })
      );
  }
}
