import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import {
  MatButtonModule,
  MatCheckboxModule,
  MatInputModule,
  MatSelectModule,
  MatListModule,
  MatCardModule,
  MatTabsModule,
  MatDatepickerModule,
  MatNativeDateModule
} from '@angular/material';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { GenreComponent } from './components/genre/genre.component';
import { UserComponent } from './components/user/user.component';
import { MoviesComponent } from './components/movies/movies.component';
import { MovieReservationComponent } from './components/movie-reservation/movie-reservation.component';
import { GenreFormComponent } from './components/genre/genre-form/genre-form.component';
import { UserFormComponent } from './components/user/user-form/user-form.component';
import { MovieFormComponent } from './components/movies/movie-form/movie-form.component';
import { ReservationFormComponent } from './components/movie-reservation/reservation-form/reservation-form.component';

@NgModule({
  declarations: [
    AppComponent,
    GenreComponent,
    UserComponent,
    MoviesComponent,
    MovieReservationComponent,
    GenreFormComponent,
    UserFormComponent,
    MovieFormComponent,
    ReservationFormComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatCheckboxModule,
    MatInputModule,
    MatSelectModule,
    MatListModule,
    MatCardModule,
    MatTabsModule,
    MatDatepickerModule,
    MatNativeDateModule
  ],
  exports: [
    MatButtonModule,
    MatCheckboxModule,
    MatInputModule,
    MatSelectModule,
    MatListModule,
    MatCardModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
