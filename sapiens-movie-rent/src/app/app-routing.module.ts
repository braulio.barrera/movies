import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GenreComponent } from './components/genre/genre.component';
import { MovieReservationComponent } from './components/movie-reservation/movie-reservation.component';
import { MoviesComponent } from './components/movies/movies.component';
import { UserComponent } from './components/user/user.component';

const routes: Routes = [
  {path: 'genres', component: GenreComponent},
  {path: 'users', component: UserComponent},
  {path: 'movies', component: MoviesComponent},
  {path: 'reservations', component: MovieReservationComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
