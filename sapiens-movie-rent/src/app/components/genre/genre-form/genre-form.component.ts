import { Component, OnInit, Input } from '@angular/core';
import { FormControl, FormGroup, Validators, FormBuilder } from '@angular/forms';

import { GenreService } from '../../../services/genre-service/genre.service';
import { Genre } from '../../../models/genre';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-genre-form',
  templateUrl: './genre-form.component.html',
  styleUrls: ['./genre-form.component.css']
})
export class GenreFormComponent implements OnInit {
  public genreForm: FormGroup;
  public formBuilder: FormBuilder;
  @Input() genre: Observable<Genre>;
  private eventsSubs: any;
  private editMode = false;
  constructor(private genreService: GenreService) { }

  ngOnInit() {
    this.createForm();
    this.eventsSubs = this.genre.subscribe((genre) => {
      this.onGenreRetrieved(genre);
      this.editMode = !this.editMode;
    })
  }

  createForm(){
    this.genreForm = new FormGroup({
        id: new FormControl(0),
        name: new FormControl(null, Validators.required)
      }
    )
  }

  onSubmit(){
    var model = new Genre(this.genreForm.value);
    if(this.editMode){
      this.genreService.update(model).subscribe();
    } else {
      this.genreService.create(model).subscribe();
    }    
    
  }

  onGenreRetrieved(genre: Genre) {
    this.genreForm.patchValue({
      id: genre.id,
      name: genre.name,
    })
  }

}
