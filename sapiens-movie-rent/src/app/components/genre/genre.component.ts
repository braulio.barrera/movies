import { Component, OnInit } from '@angular/core';

import { GenreService } from '../../services/genre-service/genre.service';
import { Genre } from '../../models/genre';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-genre',
  templateUrl: './genre.component.html',
  styleUrls: ['./genre.component.css']
})
export class GenreComponent implements OnInit {
  public genres: Genre[];
  public hideForm = true;
  private eventSubject: Subject<Genre> = new Subject<Genre>();
  constructor(private genreService: GenreService) { }

  ngOnInit() {
    this.getGenres();
  }

  getGenres(){
    this.genreService.getGenres()
    .subscribe(genres => this.genres = genres);
  }

  showForm(){
    this.hideForm = !this.hideForm;
  }

  editGenre(genre){    
    this.eventSubject.next(genre);
  }

}
