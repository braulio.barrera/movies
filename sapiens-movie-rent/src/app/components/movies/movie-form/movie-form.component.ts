import { Component, OnInit, Input } from '@angular/core';
import { FormControl, FormGroup, Validators, FormBuilder } from '@angular/forms';

import { MovieService } from '../../../services/movies-service/movie.service';
import { Movie } from '../../../models/movie';

import { GenreService } from '../../../services/genre-service/genre.service';
import { Genre } from '../../../models/genre';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-movie-form',
  templateUrl: './movie-form.component.html',
  styleUrls: ['./movie-form.component.css']
})
export class MovieFormComponent implements OnInit {
  @Input() movie: Observable<Movie>;
  public movieForm: FormGroup;
  public formBuilder: FormBuilder;
  public genres: Genre[];
  private eventsSubs: any;
  constructor(private movieService: MovieService, private genreService: GenreService) { }

  ngOnInit() {
    this.createForm()
    this.getGenres();
    this.eventsSubs = this.movie.subscribe((movie) => {
      this.onMovieRetrieved(movie);
    })

  }

  createForm() {
    this.movieForm = new FormGroup({
      id: new FormControl(0),
      title: new FormControl(null, Validators.required),
      moviewYear: new FormControl(null, Validators.required),
      genreId: new FormControl(null, Validators.required),
      directorName: new FormControl(null, Validators.required),
      rating: new FormControl(null, Validators.required),
      available: new FormControl(null, Validators.required),
      actors: new FormControl(null, Validators.required),
    }
    )
  }

  onSubmit() {
    var model = new Movie(this.movieForm.value);
    if (this.movie) {
      this.movieService.update(model)
        .subscribe(resp =>
          console.log(resp)
        );
    } else {
      this.movieService.create(model)
        .subscribe(resp =>
          console.log(resp)
        );
    }
  }

  getGenres() {
    this.genreService.getGenres()
      .subscribe(genres => this.genres = genres);
  }

  onMovieRetrieved(movie: Movie) {
    this.movieForm.patchValue({
      id: movie.id,
      title: movie.title,
      moviewYear: movie.moviewYear,
      genreId: movie.genreId,
      directorName: movie.directorName,
      rating: movie.rating,
      available: movie.available,
      actors: movie.actors,
    })
  }
}
