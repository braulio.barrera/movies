import { Component, OnInit } from '@angular/core';

import { MovieService } from '../../services/movies-service/movie.service';
import { Movie } from  '../../models/movie';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-movies',
  templateUrl: './movies.component.html',
  styleUrls: ['./movies.component.css'],
  
})
export class MoviesComponent implements OnInit {
  public movies: Movie[];
  private eventSubject: Subject<Movie> = new Subject<Movie>();
  public hideForm = true;
  constructor(private movieService: MovieService) { }

  ngOnInit() {
    this.getMovies();
  }

  getMovies(){
    this.movieService.getMovies().subscribe(movies => this.movies = movies);
  }

  showForm(){
    this.hideForm = !this.hideForm;
  }

  editMovie(movie){    
    this.eventSubject.next(movie);
  }
}
