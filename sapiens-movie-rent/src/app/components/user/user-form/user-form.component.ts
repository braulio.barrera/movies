import { Component, OnInit, Input } from '@angular/core';
import { FormControl, FormGroup, Validators, FormBuilder } from '@angular/forms';

import { UserService } from '../../../services/user-service/user.service';
import { User } from '../../../models/user';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.css']
})
export class UserFormComponent implements OnInit {
  @Input() userOb: Observable<User>;
  private eventSubs: any;
  public user: User;
  public userForm: FormGroup;
  public formBuilder: FormBuilder;
  public editMode = false;
  constructor(private userService: UserService) { }

  ngOnInit() {
    this.createForm()
    this.eventSubs = this.userOb.subscribe((user) => {
      this.onUserRetrieved(user);
      this.editMode = !this.editMode;
    })
  }

  createForm(){
    this.userForm = new FormGroup({
        userId: new FormControl(0),
        name: new FormControl(null, Validators.required),
        email: new FormControl(null, [Validators.required, Validators.email])
      }
    )
  }

  onSubmit(){
    var model = new User(this.userForm.value);
    if(this.editMode){
      this.userService.update(model).subscribe();
    } else {
      this.userService.create(model).subscribe();
    }    
  }

  onUserRetrieved(user: User){
    this.user = user;
    this.userForm.patchValue({
        userId: this.user.userId,
        name: this.user.name,
        email: this.user.email
    })
  }
}
