import { Component, OnInit } from '@angular/core';

import { UserService } from '../../services/user-service/user.service';
import { User } from '../../models/user';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {
  public users: User[];
  public hideForm = true;
  private eventUserSubject: Subject<User> = new Subject<User>();
  constructor(private userService: UserService) { }

  ngOnInit() {
    this.getGenres();
  }

  getGenres(){
    this.userService.getUsers()
    .subscribe(users => this.users = users);
  }

  showForm(){
    this.hideForm = !this.hideForm;
  }

  editUser(user){
    user.editMode = true;
    this.eventUserSubject.next(user);
  }
}
