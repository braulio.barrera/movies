import { Component, OnInit } from '@angular/core';

import { MovieReservationService } from '../../services/movie-reservation-service/movie-reservation.service';
import { MovieReservation } from  '../../models/movie-reservation';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-movie-reservation',
  templateUrl: './movie-reservation.component.html',
  styleUrls: ['./movie-reservation.component.css']
})
export class MovieReservationComponent implements OnInit {

  public reservations: MovieReservation[];
  public hideForm = true;
  private eventReserSubject: Subject<MovieReservation> = new Subject<MovieReservation>();
  constructor(private reservationService: MovieReservationService) { }

  ngOnInit() {
    this.getReservations();
  }

  getReservations(){
    this.reservationService.getReservations()
    .subscribe(reservations => this.reservations = reservations);
  }

  showForm(){
    this.hideForm = !this.hideForm;
  }

  onEditReservation(reservation){
    this.eventReserSubject.next(reservation);
  }
}
