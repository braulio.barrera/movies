import { Component, OnInit, Input } from '@angular/core';
import { FormControl, FormGroup, Validators, FormBuilder } from '@angular/forms';

import { MovieReservationService } from '../../../services/movie-reservation-service/movie-reservation.service';
import { MovieReservation } from  '../../../models/movie-reservation';

import { UserService } from '../../../services/user-service/user.service';
import { User } from '../../../models/user';

import { MovieService } from '../../../services/movies-service/movie.service';
import { Movie } from  '../../../models/movie';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-reservation-form',
  templateUrl: './reservation-form.component.html',
  styleUrls: ['./reservation-form.component.css']
})
export class ReservationFormComponent implements OnInit {
  @Input() movieReservation: Observable<MovieReservation>;
  public movieReservationForm: FormGroup;
  public formBuilder: FormBuilder;
  public users: User[];
  public movies: Movie[];
  private eventsSubs: any;
  private editMode= false;

  constructor(private movieReservationService: MovieReservationService, 
    private userService: UserService,
    private movieService: MovieService) { }

  ngOnInit() {
    this.createForm()
    this.getUsers();
    this.getMovies();
    this.eventsSubs = this.movieReservation.subscribe((reservation) => {
      this.onReservationRetrieved(reservation);
      this.editMode = !this.editMode;
    })
  }

  createForm(){
    this.movieReservationForm = new FormGroup({
        reservationId: new FormControl(0),
        reservationFrom: new FormControl(null, Validators.required),
        reservationTo: new FormControl(null, Validators.required),
        returned: new FormControl(false, Validators.required),
        userId: new FormControl(null, Validators.required),
        movieId: new FormControl(null, Validators.required)
      }
    )
  }

  onSubmit(){
    var model = new MovieReservation(this.movieReservationForm.value);
    if(this.editMode){
      model.returned = false;
      this.movieReservationService.update(model).subscribe();
    } else {
      this.movieReservationService.create(model).subscribe();
    }
    
  }
  getUsers(){
    this.userService.getUsers()
    .subscribe(users => this.users = users);
  }

  getMovies(){
    this.movieService.getMovies()
    .subscribe(movies => this.movies = movies);
  }

  onReservationRetrieved(reservation: MovieReservation) {
    this.movieReservationForm.patchValue({
      reservationId: reservation.reservationId,
      reservationFrom: reservation.reservationFrom,
      reservationTo: reservation.reservationTo,
      returned: reservation.returned,
      userId: reservation.userId,
      movieId: reservation.movieId
    })
  }
}
