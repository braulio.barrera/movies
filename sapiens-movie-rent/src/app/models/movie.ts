import { Genre } from './genre';

export class Movie{
    id?: number;        
    title: string;
    moviewYear: number;
    genreId: Genre;
    directorName: string;
    rating: string;
    available: string;
    actors: string;

    constructor(options: any){
        this.id = options.id === null ? 0 : options.id;
        this.title = options.title;
        this.directorName = options.directorName;
        this.moviewYear = options.moviewYear;
        this.genreId = options.genreId;
        this.rating = options.rating;
        this.available = options.available;
        this.actors = options.actors;
    }
}