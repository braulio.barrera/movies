export class User{
    userId?: number; 
    name: string;
    email: string;

    constructor(options: any){
        this.userId = options.userId === null ? 0 : options.userId;
        this.name = options.name;
        this.email = options.email;
    }
}