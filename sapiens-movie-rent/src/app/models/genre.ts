export class Genre{
    id?: number; 
    name: string;

    constructor(options: any){
        this.id = options.id === null ? 0 : options.id;
        this.name = options.name;
    }
}