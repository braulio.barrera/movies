import { User } from './user';
import { Movie } from './movie';

export class MovieReservation{
    reservationId?: number;
    reservationFrom: Date;
    reservationTo: Date;
    returned: boolean;
    userId: User;
    movieId: Movie;

    constructor(options: any){
        this.reservationId = options.reservationId === null ? 0 : options.reservationId;
        this.reservationFrom = options.reservationFrom;
        this.reservationTo = options.reservationTo;
        this.returned = options.returned;
        this.userId = options.userId;
        this.movieId = options.movieId;
    }
}